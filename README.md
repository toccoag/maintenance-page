# Maintenance Page

See documentation available under https://tocco-docs.readthedocs.io/en/latest/devops/maintenance/maintenance_page.html

# Deloyment

## Debian Packages

Debian packages are build automatically when pushing to *master*. Increment
version in [root/DEBIAN/control](root/DEBIAN/control) to release a new
version to https://mirror.tocco.ch.

The [root/](root) directory contains everything included in the Debian package.

## Docker

Docker image is automatically updated when a change is pushed to *master*.

The [docker/](docker) directory contains everything included in the Docker image.
